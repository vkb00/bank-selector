module.exports = {
    'settings': {
        'react': {
            'version': 'detect',
        },
    },
    'env': {
        'browser': true,
        'es2021': true,
    },
    'extends': [
        'plugin:react/recommended',
        'google',
    ],
    'parser': '@typescript-eslint/parser',
    'parserOptions': {
        'ecmaFeatures': {
            'jsx': true,
        },
        'ecmaVersion': 'latest',
        'sourceType': 'module',
    },
    'plugins': [
        'react',
        '@typescript-eslint',
    ],
    'rules': {
        'indent': ['error', 4],
        'operator-linebreak': ['error', 'before'],
        'max-len': 'off',
        'react/display-name': 'off',
        'require-jsdoc': 'off',
        'valid-jsdoc': 'off',
        'new-cap': 'off',
        'react/prop-types': 'off',
        'react/jsx-uses-react': 'off',
        'comma-dangle': 'off',
        'react/react-in-jsx-scope': 'off',
    },
};
