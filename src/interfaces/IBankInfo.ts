export interface BankInfo {
    img: string;
    name: string;
    description?: string;
    isOurs?: boolean;
    isRecent?: boolean;
}
