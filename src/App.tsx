
import './App.css';
import BankSelector from './components/BankSelector/BankSelector';
import data from './data.json';
import type {BankInfo} from './interfaces';

function App() {
    const responce:BankInfo[] = data;
    return (
        <BankSelector banks={responce} />
    );
}

export default App;
