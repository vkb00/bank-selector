import './SearchBanks.scss';
import {useState} from 'react';
import type {BankInfo} from '@base/interfaces';
import {BankItem} from '../BankItem/BankItem';

export interface SearchBanksProps {
    handleClickToBank: (bank: BankInfo) => void;
    otherBanks: BankInfo[];
}

export function SearchBanks({otherBanks, handleClickToBank}: SearchBanksProps) {
    const [textInput, setTextInput] = useState<string>('');
    const [resultSearchArray, setResultSearchArray] = useState<BankInfo[] | null>(otherBanks);

    const handleSearchBanks = (e: React.ChangeEvent<HTMLInputElement>) => {
        const searchText = e.target.value;
        const filterResult = otherBanks.filter(({name}) => name.toLowerCase().includes(searchText.toLowerCase()));
        setTextInput(searchText);
        setResultSearchArray(filterResult);
        console.table(filterResult);
    };
    return (
        <>
            <h2>Другие банки</h2>
            <input type="text" value={textInput} placeholder="Поиск" onChange={handleSearchBanks} />
            {resultSearchArray && resultSearchArray.map((bank: BankInfo) => (
                <BankItem clickToBank={handleClickToBank} key={bank.name} bankInfo={bank} />))}
        </>
    );
}


