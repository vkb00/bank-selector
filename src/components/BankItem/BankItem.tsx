import './BankItem.scss';
import {BankInfo} from '@base/interfaces';

export interface BankItemProps {


    bankInfo: BankInfo;
    clickToBank: (bank: BankInfo) => void;
}

export function BankItem({bankInfo, clickToBank}: BankItemProps) {
    const handleClickToBank = () => {
        clickToBank(bankInfo);
    };
    return (
        <div onClick={handleClickToBank} className="bank">
            <img src={bankInfo.img} alt="" />
            <h2>{bankInfo.name}</h2>
        </div>
    );
}
