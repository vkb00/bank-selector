import {HTMLAttributes, useState} from 'react';
import {BankItem} from '../BankItem/BankItem';
import {SearchBanks} from '../SearchBanks/SearchBanks';
import type {BankInfo} from '../../interfaces';

export interface BankSelectorProps extends HTMLAttributes<HTMLElement> {
    banks: BankInfo[];
}

export function BankSelector({banks}: BankSelectorProps) {
    const [showBanks, setIsopenBanks] = useState(false);
    const [chosenBank, setChosenBank] = useState<BankInfo | null>(null);
    const oursBanks = banks.filter((bank: BankInfo) => bank.isOurs);
    const recentBanks = banks.filter((bank: BankInfo) => bank.isRecent);
    const otherBanks = banks.filter((bank: BankInfo) => !bank.isRecent && !bank.isOurs);
    const openBanks = () => {
        setIsopenBanks(!showBanks);
    };
    const handleClickToBank = (bank: BankInfo) => {
        setChosenBank(bank);
        setIsopenBanks(!showBanks);
    };
    const initialBankInfo = {
        img: 'https://cdn.icon-icons.com/icons2/1947/PNG/512/4635002-bank_122546.png',
        name: 'Выберите банк получателя',
    };
    return (
        <>
            <BankItem clickToBank={openBanks} bankInfo={chosenBank ?? initialBankInfo} />
            {showBanks
                && (
                    <>
                        {oursBanks.map((bank: BankInfo) => (
                            <BankItem clickToBank={handleClickToBank} key={bank.name} bankInfo={bank} />))}
                        {recentBanks.map((bank: BankInfo) => (
                            <BankItem clickToBank={handleClickToBank} key={bank.name} bankInfo={bank} />))}
                        <SearchBanks handleClickToBank={handleClickToBank} otherBanks={otherBanks} />
                    </>
                )}

        </>
    );
}

export default BankSelector;
